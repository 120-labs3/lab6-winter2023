import java.util.Scanner;
public class Jackpot{
	public static void main(String [] args){
		
		Scanner input = new Scanner(System.in);
		System.out.println("Hello player \n");

		boolean playAgain = true;
		int wins = 0;

		while(playAgain){
		
			Board game = new Board();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
				
				while (!gameOver){
					System.out.println(game);
					game.playATurn();
					if(game.playATurn()){
						gameOver = true;
					}
					else{
						numOfTilesClosed++;
					}
				}
				if(numOfTilesClosed>= 7){
					System.out.println("WINNER");
					wins++;
				}
				else{
					System.out.println("Rip you lost");
				}
				System.out.println("Do you want to play again?");
				char replay = input.next().charAt(0);
				if (replay != 'y'){
					playAgain = false;
				}
			}
			if (wins != 1){
				System.out.println("You've won " + wins + " time!");
			}
			else{
				System.out.println("You've won once");
			}
	}
}