public class Board{
	
	private Die dice1;
	private Die dice2;
	private boolean[] tiles;
	
	public Board(){	
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.tiles = new boolean[12];
		for(int i = 0;i<this.tiles.length;i++){
			tiles[i] = false;
		}
	}
	
	public String toString(){
		String values = "";
		
		for(int i = 0;i<this.tiles.length;i++){
			if (tiles[i] == false){
				values += " "+ (i +1) + " ";
			}
			else{
				values += " X ";
			}
		}
		return values;
		
	}
	public boolean playATurn(){
		
		dice1.roll();
		dice2.roll();
		
		System.out.println(dice1);
		System.out.println(dice2);
		
		int sum = dice1.getValue() + dice2.getValue();
		
		if(!(this.tiles[sum-1])){ //Checks if the sum tile is open aka false
			this.tiles[sum-1] = true;
			System.out.println("\nClosing tile number: " + sum + "\n");
			return false;
		}
		else if(!(this.tiles[dice1.getValue() - 1])){ //Check if the value of the 1st die tile is open aka false
			this.tiles[dice1.getValue() -1] = true;
			System.out.println("\nClosing tile number: " + dice1.getValue()+ "\n");
			return false;
		}
		else if(!(this.tiles[dice2.getValue() - 1])){ //Check if the value of the 1nd die tile is open aka false
			this.tiles[dice2.getValue() -1] = true;
			System.out.println("\nClosing tile number: " + dice2.getValue()+ "\n");
			return false;
		}		
		else{
			System.out.println("\nAll the tiles for these values are already shut");
			return true;
		}
		
		
	}
	
}