import java.util.Random;
public class Die{
	
	private int face;
	private Random numbers;// Die Object
	
	public Die(){ //Constructor Default Value of 1
		this.face = 1;
		this.numbers = new Random();
	}
	
	public void roll(){
		this.face = this.numbers.nextInt(6) + 1;
	}
	
	public int getValue(){
		return this.face;
	}
	
	public String toString(){
		return "The rolled value is: " + getValue();
	}
}